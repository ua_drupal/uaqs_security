api = 2
core = 7.x

defaults[projects][subdir] = contrib

; autoban contrib module.
projects[autoban][version] = 1.8

; blocked_ips_expire contrib module
projects[blocked_ips_expire][version] = 1.0

; honeypot contrib module.
projects[honeypot][version] = 1.25
