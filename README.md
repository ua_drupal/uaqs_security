# UAQS Security #

## Summary
This module provides additional security features for Drupal sites.

## Building
Start at the root of your site's codebase and follow these steps:

1.  Clone this module into your codebase:
    *  `git clone https://bitbucket.org/ua_drupal/uaqs_security.git sites/all/modules/uaqs_security`
2.  Delete the `.git` directory from the module
    *  `rm -rf sites/all/modules/uaqs_security/.git`
3.  Run drush make to build the module
    * `drush make -y --no-core sites/all/modules/uaqs_security/uaqs_security.make`
4.  Now the module and dependencies are built and can be committed to your site's codebase

## Features
* Flood support for sent mail
    * Use the variables `uaqs_security_send_mail_ip_limit` and `uaqs_security_send_mail_ip_window` to control how aggressive flood protection is. Default is 5 messages allowed per IP per hour
